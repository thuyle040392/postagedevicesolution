﻿namespace PostageDevice.UnitTests.Application
{
    public class PostalDeviceServiceTests : IDisposable
    {
        private readonly Mock<IPostalDeviceRepository> _postalDeviceRepositoryMock;
        private readonly IPostalDeviceService _postalDeviceService;

        public PostalDeviceServiceTests()
        {
            _postalDeviceRepositoryMock = new Mock<IPostalDeviceRepository>();
            _postalDeviceService = new PostalDeviceService(_postalDeviceRepositoryMock.Object);
        }


        [Fact]
        public async Task DeclareNewPostageDevice_ShouldReturnNewPostalDevice_WhenCalledWithValidPostalIdentifier()
        {
            var mockRepo = new Mock<IPostalDeviceRepository>();
            var service = new PostalDeviceService(mockRepo.Object);
            var postalIdentifier = "AB12345";

            mockRepo.Setup(r => r.GetByPostalIdentifierAsync(postalIdentifier))
                    .ReturnsAsync((PostalDevice)null);
            mockRepo.Setup(r => r.AddAsync(It.IsAny<PostalDevice>()))
                    .Returns(Task.CompletedTask);
            mockRepo.Setup(r => r.BeginTransactionAsync())
                    .ReturnsAsync(Mock.Of<IDbContextTransaction>());

            var result = await service.DeclareNewPostageDevice(postalIdentifier);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(postalIdentifier, result.PostalIdentifier);
        }

        [Fact]
        public async Task DeclareNewPostageDevice_ShouldThrowInvalidOperationException_WhenCalledWithDuplicatePostalIdentifier()
        {
            var mockRepo = new Mock<IPostalDeviceRepository>();
            var service = new PostalDeviceService(mockRepo.Object);
            var postalIdentifier = "AB12345";
            var existingDevice = new PostalDevice(postalIdentifier);

            mockRepo.Setup(r => r.GetByPostalIdentifierAsync(postalIdentifier))
                    .ReturnsAsync(existingDevice);
            mockRepo.Setup(r => r.BeginTransactionAsync())
                    .ReturnsAsync(Mock.Of<IDbContextTransaction>());

            await Assert.ThrowsAsync<InvalidOperationException>(() => service.DeclareNewPostageDevice(postalIdentifier));
        }

        [Fact]
        public async Task DeclareNewPostageDevice_ShouldRollbackTransaction_WhenExceptionOccurs()
        {
            // Arrange
            var mockRepo = new Mock<IPostalDeviceRepository>();
            var service = new PostalDeviceService(mockRepo.Object);
            var postalIdentifier = "AB12345";

            mockRepo.Setup(r => r.GetByPostalIdentifierAsync(postalIdentifier))
                    .ReturnsAsync((PostalDevice)null);
            var mockTransaction = new Mock<IDbContextTransaction>();
            mockRepo.Setup(r => r.BeginTransactionAsync())
                    .ReturnsAsync(mockTransaction.Object);
            mockRepo.Setup(r => r.AddAsync(It.IsAny<PostalDevice>()))
                    .Throws(new Exception());

            // Act
            Exception ex = await Assert.ThrowsAsync<Exception>(() => service.DeclareNewPostageDevice(postalIdentifier));

            // Assert
            mockTransaction.Verify(t => t.RollbackAsync(default), Times.Once);
        }

        [Fact]
        public async Task GetListOfPostageDevices_ShouldReturnPostalDeviceDtos_WhenPostalDevicesExist()
        {
            // Arrange
            var postalDevices = new List<PostalDevice>
                {
                    new PostalDevice("AB12345"),
                    new PostalDevice("CD67890")
                };
            _postalDeviceRepositoryMock.Setup(p => p.GetAllAsync())
                .ReturnsAsync(postalDevices);

            // Act
            var result = (await _postalDeviceService.GetListOfPostageDevices()).ToList();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(postalDevices.Count, result.Count);
        }

        [Fact]
        public async Task GetListOfPostageDevices_ShouldReturnEmptyList_WhenNoPostalDevicesExist()
        {
            // Arrange
            _postalDeviceRepositoryMock.Setup(p => p.GetAllAsync())
                .ReturnsAsync(new List<PostalDevice>());

            // Act
            var result = await _postalDeviceService.GetListOfPostageDevices();

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetByPostalIdentifierAsync_ShouldReturnPostalDevice_WhenExists()
        {
            // Arrange
            var postalIdentifier = "AB12345";
            var postalDevice = new PostalDevice(postalIdentifier);

            _postalDeviceRepositoryMock.Setup(r => r.GetByPostalIdentifierAsync(postalIdentifier))
                .ReturnsAsync(postalDevice);

            // Act
            var result = await _postalDeviceService.GetPostageDeviceInformation(postalIdentifier);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(postalIdentifier, result.PostalIdentifier);
        }

        [Fact]
        public async Task GetByPostalIdentifierAsync_ShouldReturnNull_WhenDoesNotExist()
        {
            // Arrange
            var postalIdentifier = "XY98765";

            _postalDeviceRepositoryMock.Setup(r => r.GetByPostalIdentifierAsync(postalIdentifier))
                .ReturnsAsync((PostalDevice)null);

            // Act
            var result = await _postalDeviceService.GetPostageDeviceInformation(postalIdentifier);

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task WithdrawPostalIdentifier_ShouldSetStatusToWithdrawn_WhenPostalDeviceExists()
        {
            // Arrange
            var postalIdentifier = "AB12345";
            var postalDevice = new PostalDevice(postalIdentifier);

            _postalDeviceRepositoryMock.Setup(r => r.GetByPostalIdentifierAsync(postalIdentifier))
                .ReturnsAsync(postalDevice);
            _postalDeviceRepositoryMock.Setup(r => r.UpdateAsync(postalDevice)).Returns(Task.CompletedTask);

            await _postalDeviceService.WithdrawPostalIdentifier(postalIdentifier);

            Assert.Equal(Status.Withdrawn, postalDevice.Status);
        }

        [Fact]
        public async Task WithdrawPostalIdentifier_ShouldThrowException_WhenPostalDeviceDoesNotExist()
        {
            // Arrange
            var postalIdentifier = "XY98765";
            _postalDeviceRepositoryMock.Setup(r => r.GetByPostalIdentifierAsync(postalIdentifier))
                .ReturnsAsync((PostalDevice)null);

            // Act & Assert
            await Assert.ThrowsAsync<InvalidOperationException>(async () =>
                await _postalDeviceService.WithdrawPostalIdentifier(postalIdentifier));
        }



        public void Dispose()
        {
            _postalDeviceRepositoryMock.Reset();
        }
    }
}
