﻿using PostageDevice.ImprintApiSdk.ImprintApi;
using PostageDevice.Service.DTOs;
using System.Net;
using System.Text;
using System.Text.Json;

namespace PostageDevice.Tests.Application
{
    public class StampServiceTests
    {
        [Fact]
        public async Task GenerateStampAsync_ShouldReturnStampResponseDto_WhenSuccessful()
        {
            var mockPostalDeviceRepository = new Mock<IPostalDeviceRepository>();
            var mockImprintApiClient = new Mock<IImprintApiClient>();
            var postalDevice = new PostalDevice("AB12745", Status.Installed);
            var stampRequestDto = new StampRequestDto { PostalIdentifier = "AB12745", ItemCount = 1, PostageValue = 5 };
            var expectedResponse = new StampResponseDto { StampImageBase64 = "SomeBase64String" };

            mockPostalDeviceRepository.Setup(x => x.GetByPostalIdentifierAsync("AB12745"))
                .ReturnsAsync(postalDevice);
            mockImprintApiClient.Setup(x => x.GenerateImprintAsync(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<decimal>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(JsonSerializer.Serialize(expectedResponse), Encoding.UTF8, "application/json")
                });
            mockPostalDeviceRepository.Setup(x => x.UpdateRegistersAsync(It.IsAny<PostalDevice>(), It.IsAny<int>(), It.IsAny<decimal>()))
                .Returns(Task.CompletedTask);

            var stampService = new StampService(mockImprintApiClient.Object, mockPostalDeviceRepository.Object);

            // Act
            var result = await stampService.GenerateStampAsync(stampRequestDto);

            // Assert
            Assert.Equal(expectedResponse.StampImageBase64, result.StampImageBase64);
            mockPostalDeviceRepository.Verify(x => x.UpdateRegistersAsync(postalDevice, 1, 5), Times.Once);
        }

        [Fact]
        public async Task GenerateStampAsync_ShouldThrowInvalidOperationException_WhenPostalDeviceNotFound()
        {
            // Arrange
            var mockPostalDeviceRepository = new Mock<IPostalDeviceRepository>();
            var mockImprintApiClient = new Mock<IImprintApiClient>();
            var serializerOptions = new JsonSerializerOptions();

            mockPostalDeviceRepository.Setup(x => x.GetByPostalIdentifierAsync(It.IsAny<string>()))
                .ReturnsAsync((PostalDevice)null);

            var stampService = new StampService(mockImprintApiClient.Object, mockPostalDeviceRepository.Object);

            await Assert.ThrowsAsync<InvalidOperationException>(() => stampService.GenerateStampAsync(new StampRequestDto()));
        }

        [Fact]
        public async Task GenerateStampAsync_ShouldThrowInvalidOperationException_WhenPostalDeviceStatusNotInstalled()
        {
            // Arrange
            var mockPostalDeviceRepository = new Mock<IPostalDeviceRepository>();
            var mockImprintApiClient = new Mock<IImprintApiClient>();
            var serializerOptions = new JsonSerializerOptions();
            var postalDevice = new PostalDevice("AB12745", Status.Withdrawn);

            mockPostalDeviceRepository.Setup(x => x.GetByPostalIdentifierAsync(It.IsAny<string>()))
                .ReturnsAsync(postalDevice);

            var stampService = new StampService(mockImprintApiClient.Object, mockPostalDeviceRepository.Object);

            await Assert.ThrowsAsync<InvalidOperationException>(() => stampService.GenerateStampAsync(new StampRequestDto()));
        }

        [Fact]
        public async Task GenerateStampAsync_ShouldThrowInvalidOperationExceptionAndUpdateStatus_WhenImprintAPIReturnsForbidden()
        {
            // Arrange
            var mockPostalDeviceRepository = new Mock<IPostalDeviceRepository>();
            var mockImprintApiClient = new Mock<IImprintApiClient>();
            var serializerOptions = new JsonSerializerOptions();
            var postalDevice = new PostalDevice("AB12745", Status.Installed);

            mockPostalDeviceRepository.Setup(x => x.GetByPostalIdentifierAsync(It.IsAny<string>()))
                .ReturnsAsync(postalDevice);
            mockImprintApiClient.Setup(x => x.GenerateImprintAsync(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<decimal>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.Forbidden));
            mockPostalDeviceRepository.Setup(x => x.UpdateStatusAsync(postalDevice, Status.Blocked))
                .Returns(Task.CompletedTask);

            var stampService = new StampService(mockImprintApiClient.Object, mockPostalDeviceRepository.Object);

            await Assert.ThrowsAsync<InvalidOperationException>(() => stampService.GenerateStampAsync(new StampRequestDto()));
            mockPostalDeviceRepository.Verify(x => x.UpdateStatusAsync(postalDevice, Status.Blocked), Times.Once);
        }

        [Fact]
        public async Task GenerateStampAsync_ShouldThrowInvalidOperationException_WhenImprintAPIReturnsNonSuccess()
        {
            // Arrange
            var mockPostalDeviceRepository = new Mock<IPostalDeviceRepository>();
            var mockImprintApiClient = new Mock<IImprintApiClient>();
            var serializerOptions = new JsonSerializerOptions();
            var postalDevice = new PostalDevice("AB12745", Status.Installed);

            mockPostalDeviceRepository.Setup(x => x.GetByPostalIdentifierAsync(It.IsAny<string>()))
                .ReturnsAsync(postalDevice);
            mockImprintApiClient.Setup(x => x.GenerateImprintAsync(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<decimal>()))
                .ReturnsAsync(new HttpResponseMessage(HttpStatusCode.BadRequest));

            var stampService = new StampService(mockImprintApiClient.Object, mockPostalDeviceRepository.Object);

            await Assert.ThrowsAsync<InvalidOperationException>(() => stampService.GenerateStampAsync(new StampRequestDto()));
        }
    }
}
