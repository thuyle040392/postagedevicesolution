﻿using PostageDevice.Service.DTOs;

namespace PostageDevice.Tests.Helpers
{
    public static class Helper
    {
        public static class TestHelper
        {
            public static void AssertPostalDeviceDtoListsAreEqual(IEnumerable<PostalDeviceDto> expected, IEnumerable<PostalDeviceDto> actual)
            {
                var expectedList = expected.ToList();
                var actualList = actual.ToList();

                Assert.Equal(expectedList.Count, actualList.Count);

                for (int i = 0; i < expectedList.Count; i++)
                {
                    Assert.Equal(expectedList[i].PostalIdentifier, actualList[i].PostalIdentifier);
                }
            }
        }

    }
}
