﻿namespace PostageDevice.UnitTests.Infrastructure
{
    public class PostalDeviceRepositoryTests : IDisposable
    {
        private readonly PostalDeviceRepository _repository;
        private readonly PostageDeviceDbContext _context;

        public PostalDeviceRepositoryTests()
        {
            var options = new DbContextOptionsBuilder<PostageDeviceDbContext>()
                .UseInMemoryDatabase(databaseName: "PostalDeviceTestDb")
                .Options;

            _context = new PostageDeviceDbContext(options);
            _repository = new PostalDeviceRepository(_context);
        }

        [Fact]
        public async Task AddAsync_ShouldCreateNewPostalDevice_WhenPostalIdentifierIsValid()
        {
            // Arrange
            var postalIdentifier = "AB12345";
            var postalDevice = new PostalDevice(postalIdentifier);

            // Act
            await _repository.AddAsync(postalDevice);
            await _context.SaveChangesAsync();

            // Assert
            var createdDevice = await _context.PostalDevices.FindAsync(postalDevice.Id);
            Assert.NotNull(createdDevice);
            Assert.Equal(postalIdentifier, createdDevice.PostalIdentifier);
        }

        //[Fact]
        //public async Task AddAsync_ShouldThrowDbUpdateException_WhenPostalIdentifierIsDuplicate()
        //{
        //    // Arrange
        //    var postalIdentifier = "CD67890";
        //    var postalDevice1 = new PostalDevice(postalIdentifier);
        //    var postalDevice2 = new PostalDevice(postalIdentifier);

        //    await _repository.AddAsync(postalDevice1);
        //    await _context.SaveChangesAsync();

        //    await _repository.AddAsync(postalDevice2);
        //    await Assert.ThrowsAsync<DbUpdateException>(() => _context.SaveChangesAsync());
        //}


        [Fact]
        public async Task GetListOfPostageDevices_ShouldReturnListOfAllPostalDevices()
        {
            // Arrange
            var postalDevice1 = new PostalDevice("AB12345");
            var postalDevice2 = new PostalDevice("CD67890");
            await _repository.AddAsync(postalDevice1);
            await _repository.AddAsync(postalDevice2);
            await _context.SaveChangesAsync();

            var result = (await _repository.GetAllAsync()).ToList();

            // Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count);
            Assert.Contains(result, pd => pd.PostalIdentifier == "AB12345");
            Assert.Contains(result, pd => pd.PostalIdentifier == "CD67890");
        }

        [Fact]
        public async Task GetListOfPostageDevices_ShouldReturnEmptyList_WhenNoPostalDevicesExist()
        {
            // Act
            var result = await _repository.GetAllAsync();

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);
        }


        [Fact]
        public async Task GetByPostalIdentifierAsync_ShouldReturnPostalDevice_WhenExists()
        {
            // Arrange
            var postalIdentifier = "AB12345";
            var postalDevice = new PostalDevice(postalIdentifier);

            await _context.PostalDevices.AddAsync(postalDevice);
            await _context.SaveChangesAsync();

            // Act
            var result = await _repository.GetByPostalIdentifierAsync(postalIdentifier);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(postalIdentifier, result.PostalIdentifier);
        }

        [Fact]
        public async Task GetByPostalIdentifierAsync_ShouldReturnNull_WhenDoesNotExist()
        {
            // Arrange
            var nonExistingPostalIdentifier = "XY98765";

            var result = await _repository.GetByPostalIdentifierAsync(nonExistingPostalIdentifier);
            Assert.Null(result);
        }

        [Fact]
        public async Task UpdateAsync_ShouldUpdatePostalDeviceSuccessfully()
        {
            // Arrange
            var postalIdentifier = "AB12345";
            var initialPostalDevice = new PostalDevice(postalIdentifier);

            await _repository.AddAsync(initialPostalDevice);

            initialPostalDevice.UpdateStatus(Status.Withdrawn);

            // Act
            await _repository.UpdateAsync(initialPostalDevice);

            // Assert
            var updatedPostalDevice = await _repository.GetByPostalIdentifierAsync(postalIdentifier);
            Assert.Equal(Status.Withdrawn, updatedPostalDevice.Status);
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
