﻿namespace PostageDevice.UnitTests.Core
{
    public class PostalDeviceDomainTests
    {

        [Theory]
        [InlineData("AB1234")] // Less than 5 digits
        [InlineData("AB123456")] // More than 5 digits
        [InlineData("A12345")] // Only one letter
        [InlineData("1234567")] // No letters
        [InlineData("ABC1234")] // More than two letters
        [InlineData("")] // Empty string
        public void PostalDevice_Constructor_ShouldThrowArgumentException_WhenPostalIdentifierIsInvalid(string invalidPostalIdentifier)
        {
            var exception = Assert.Throws<ArgumentException>(() => new PostalDevice(invalidPostalIdentifier));
            Assert.Equal($"Postal Identifier must be composed of 2 letters followed by 5 numbers.", exception.Message);
        }

        [Theory]
        [InlineData("AB12345")] 
        public void Constructor_ShouldCreateObject_WithCorrectInitialState_WhenPostalIdentifierIsValid(string postalIdentifier)
        {
            // Act
            var postalDevice = new PostalDevice(postalIdentifier);

            Assert.NotNull(postalDevice);
            Assert.Equal(postalIdentifier, postalDevice.PostalIdentifier);
            Assert.Equal(Status.Installed, postalDevice.Status);
        }
    }


}
