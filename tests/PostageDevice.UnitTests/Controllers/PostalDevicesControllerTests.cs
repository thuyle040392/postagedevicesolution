﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PostageDevice.Controller.Controllers;
using PostageDevice.Service.DTOs;
using static PostageDevice.Tests.Helpers.Helper;

namespace PostageDevice.Tests.Controllers
{
    public class PostalDevicesControllerTests
    {
        [Fact]
        public async Task DeclareNewPostageDevice_ShouldReturnCreatedAtAction_WhenPostalDeviceIsCreated()
        {
            var mockService = new Mock<IPostalDeviceService>();
            var mockMapper = new Mock<IMapper>();
            var controller = new PostalDevicesController(mockService.Object, mockMapper.Object);
            var postalIdentifier = "AB12345";

            var postalDevice = new PostalDevice(postalIdentifier);
            var postalDeviceDto = new PostalDeviceDto { PostalIdentifier = postalIdentifier };

            mockService.Setup(s => s.DeclareNewPostageDevice(postalIdentifier)).ReturnsAsync(postalDevice);
            mockMapper.Setup(m => m.Map<PostalDeviceDto>(postalDevice)).Returns(postalDeviceDto);

            var result = await controller.DeclareNewPostageDevice(postalIdentifier);

            var createdAtActionResult = Assert.IsType<CreatedAtActionResult>(result);
            var returnValue = Assert.IsType<PostalDeviceDto>(createdAtActionResult.Value);
            Assert.Equal(postalIdentifier, returnValue.PostalIdentifier);
        }

        [Fact]
        public async Task GetListofPostageDevices_ShouldReturnListOfPostalDeviceDtos()
        {
            var mockService = new Mock<IPostalDeviceService>();
            var mockMapper = new Mock<IMapper>();
            var controller = new PostalDevicesController(mockService.Object, mockMapper.Object);

            var postalDevices = new List<PostalDevice> { new PostalDevice("AB12345"), new PostalDevice("CD67890") };
            var postalDeviceDtos = postalDevices.Select(p => new PostalDeviceDto { PostalIdentifier = p.PostalIdentifier });

            mockService.Setup(s => s.GetListOfPostageDevices()).ReturnsAsync(postalDevices);
            mockMapper.Setup(m => m.Map<IEnumerable<PostalDeviceDto>>(postalDevices)).Returns(postalDeviceDtos);

            var result = (await controller.GetListofPostageDevices()).ToList();
            TestHelper.AssertPostalDeviceDtoListsAreEqual(postalDeviceDtos, result);
        }

        [Fact]
        public async Task GetPostageDeviceInformation_ShouldReturnOk_WhenPostalDeviceExists()
        {
            var mockService = new Mock<IPostalDeviceService>();
            var mockMapper = new Mock<IMapper>();
            var controller = new PostalDevicesController(mockService.Object, mockMapper.Object);
            var postalIdentifier = "AB12345";

            var postalDevice = new PostalDevice(postalIdentifier);
            var postalDeviceDto = new PostalDeviceDto { PostalIdentifier = postalIdentifier };

            mockService.Setup(s => s.GetPostageDeviceInformation(postalIdentifier)).ReturnsAsync(postalDevice);
            mockMapper.Setup(m => m.Map<PostalDeviceDto>(postalDevice)).Returns(postalDeviceDto);

            var result = await controller.GetPostageDeviceInformation(postalIdentifier);

            var okResult = Assert.IsType<OkObjectResult>(result);
            var returnValue = Assert.IsType<PostalDeviceDto>(okResult.Value);
            Assert.Equal(postalIdentifier, returnValue.PostalIdentifier);
        }

        [Fact]
        public async Task GetPostageDeviceInformation_ShouldReturnNotFound_WhenPostalDeviceDoesNotExist()
        {
            var mockService = new Mock<IPostalDeviceService>();
            var mockMapper = new Mock<IMapper>();
            var controller = new PostalDevicesController(mockService.Object, mockMapper.Object);
            var postalIdentifier = "AB12345";

            mockService.Setup(s => s.GetPostageDeviceInformation(postalIdentifier)).ReturnsAsync((PostalDevice)null);
            var result = await controller.GetPostageDeviceInformation(postalIdentifier);
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async Task WithdrawPostalIdentifier_ShouldReturnNoContent()
        {
            var mockService = new Mock<IPostalDeviceService>();
            var mockMapper = new Mock<IMapper>();
            var controller = new PostalDevicesController(mockService.Object, mockMapper.Object);
            var postalIdentifier = "AB12345";

            mockService.Setup(s => s.WithdrawPostalIdentifier(postalIdentifier)).Returns(Task.CompletedTask);
            var result = await controller.WithdrawPostalIdentifier(postalIdentifier);

            Assert.IsType<OkObjectResult>(result);
        }



    }
}
