﻿using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using Newtonsoft.Json;
using PostageDevice.Service.DTOs;

namespace PostageDevice.Tests.Controllers
{
    public class PostalDevicesIntegrationTests : IClassFixture<WebApplicationFactory<Program>>
    {
        private readonly WebApplicationFactory<Program> _factory;

        public PostalDevicesIntegrationTests(WebApplicationFactory<Program> factory)
        {
            _factory = factory;
        }

        //[Fact]
        //public async Task GetListofPostageDevices_ShouldReturn200OKAndListOfPostalDeviceDtos()
        //{
        //    // Arrange
        //    var client = _factory.CreateClient();
        //    var response = await client.GetAsync("");
        //    response.EnsureSuccessStatusCode();
        //    var stringResponse = await response.Content.ReadAsStringAsync();
        //    var postalDevices = JsonConvert.DeserializeObject<IEnumerable<PostalDeviceDto>>(stringResponse);
        //    Assert.NotEmpty(postalDevices);
        //}

    }
}
