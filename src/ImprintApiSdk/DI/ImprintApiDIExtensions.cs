﻿using Microsoft.Extensions.DependencyInjection;

namespace PostageDevice.ImprintApiSdk.ImprintApi
{
    public static class ImprintApiDIExtensions
    {
        public static void AddImprintApiSdkDI(this IServiceCollection service)
        {
            service.AddScoped<IImprintApiClient, ImprintApiClient>();
        }
    }
}
