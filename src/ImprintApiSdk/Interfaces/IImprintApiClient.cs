﻿namespace PostageDevice.ImprintApiSdk.ImprintApi
{
    public interface IImprintApiClient
    {
        Task<HttpResponseMessage> GenerateImprintAsync(string postalIdentifier, int itemCount, decimal postageValue);
    }
}
