﻿using Microsoft.Extensions.Configuration;
using System.Text;
using System.Text.Json;

namespace PostageDevice.ImprintApiSdk.ImprintApi
{
    public class ImprintApiClient : IImprintApiClient
    {
        private readonly string _printApiEndpoint;
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _configuration;

        public ImprintApiClient(HttpClient httpClient, IConfiguration configuration)
        {
            _httpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _configuration = configuration;
            _printApiEndpoint = _configuration["PrintApiEndpoint"];
            if (string.IsNullOrEmpty(_printApiEndpoint))
            {
                throw new InvalidOperationException("PrintApiEndpoint is null");
            }
        }

        public async Task<HttpResponseMessage> GenerateImprintAsync(string postalIdentifier, int itemCount, decimal postageValue)
        {
            var url = $"{_printApiEndpoint}/{postalIdentifier}/imprint";
            var requestBody = JsonSerializer.Serialize(new { itemCount, postageValue });
            var content = new StringContent(requestBody, Encoding.UTF8, "application/json");
            return await _httpClient.PostAsync(url, content);
        }
    }
}
