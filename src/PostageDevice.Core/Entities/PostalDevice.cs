﻿using PostageDevice.Core.Emuns;
using System.Text.RegularExpressions;

namespace PostageDevice.Core.Entities
{
    public class PostalDevice
    {
        public int Id { get; set; }
        public string PostalIdentifier { get; set; }
        public Status Status { get; private set; }
        public int TotalItemsCount { get; private set; }
        public decimal TotalPostageValue { get; private set; }

        public PostalDevice(string postalIdentifier)
        {
            if (Validate(postalIdentifier))
            {
                PostalIdentifier = postalIdentifier;
                UpdateStatus(Status.Installed);
            }
        }

        public PostalDevice(string postalIdentifier, Status status)
        {
            if (Validate(postalIdentifier))
            {
                PostalIdentifier = postalIdentifier;
                UpdateStatus(status);
            }
        }

        public void Withdraw() => Status = Status.Withdrawn;

        public void UpdateRegisters(int itemCount, decimal postageValue)
        {
            // Logic update 
            TotalItemsCount += itemCount;
            TotalPostageValue += postageValue;
        }

        public void UpdateStatus(Status status)
        {
            // Logic update 
            Status = status;
        }

        private bool Validate(string postalIdentifier)
        {
            // Validate postalIdentifier format.
            if (!Regex.IsMatch(postalIdentifier, @"^[A-Za-z]{2}\d{5}$"))
            {
                throw new ArgumentException("Postal Identifier must be composed of 2 letters followed by 5 numbers.");
            }
            return true;
        }
    }
}
