﻿namespace PostageDevice.Core.Entities
{
    public class Stamp
    {
        public string ImageBase64 { get; set; }
    }
}
