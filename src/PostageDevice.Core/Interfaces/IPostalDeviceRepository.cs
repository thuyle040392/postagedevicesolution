﻿using Microsoft.EntityFrameworkCore.Storage;
using PostageDevice.Core.Emuns;
using PostageDevice.Core.Entities;

namespace PostageDevice.Core.Interfaces
{
    public interface IPostalDeviceRepository
    {
        Task AddAsync(PostalDevice postalDevice);
        Task<IEnumerable<PostalDevice>> GetAllAsync();
        Task<PostalDevice> GetByPostalIdentifierAsync(string postalIdentifier);
        Task UpdateAsync(PostalDevice postalDevice);
        Task<IDbContextTransaction> BeginTransactionAsync();

        Task UpdateRegistersAsync(PostalDevice postalDevice, int itemCount, decimal postageValue);
        Task UpdateStatusAsync(PostalDevice postalDevice, Status newStatus);
    }
}
