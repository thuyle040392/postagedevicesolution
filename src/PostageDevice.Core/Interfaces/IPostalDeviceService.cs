﻿using PostageDevice.Core.Entities;

namespace PostageDevice.Core.Interfaces
{
    public interface IPostalDeviceService
    {
        Task<PostalDevice> DeclareNewPostageDevice(string postalIdentifier);
        Task<IEnumerable<PostalDevice>> GetListOfPostageDevices();
        Task<PostalDevice> GetPostageDeviceInformation(string postalIdentifier);
        Task WithdrawPostalIdentifier(string postalIdentifier);
    }
}
