﻿namespace PostageDevice.Core.Emuns
{
    public enum Status
    {
        Installed,
        Blocked,
        Withdrawn
    }
}
