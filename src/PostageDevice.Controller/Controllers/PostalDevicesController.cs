﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using PostageDevice.Core.Interfaces;
using PostageDevice.Service.DTOs;

namespace PostageDevice.Controller.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PostalDevicesController : ControllerBase
    {
        private readonly IPostalDeviceService _postalDeviceService;
        private readonly IMapper _mapper;

        public PostalDevicesController(IPostalDeviceService postalDeviceService, IMapper mapper)
        {
            _postalDeviceService = postalDeviceService;
            _mapper = mapper;
        }

        [HttpPost("declareNew")]
        public async Task<IActionResult> DeclareNewPostageDevice(string postalIdentifier)
        {
            var postalDevice = await _postalDeviceService.DeclareNewPostageDevice(postalIdentifier);
            var dto = _mapper.Map<PostalDeviceDto>(postalDevice);
            return CreatedAtAction(nameof(GetPostageDeviceInformation), new { postalIdentifier = dto.PostalIdentifier }, dto);
        }

        [HttpGet("getListPostalIdentifiers")]
        public async Task<IEnumerable<PostalDeviceDto>> GetListofPostageDevices()
        {
            var postalDevices = await _postalDeviceService.GetListOfPostageDevices();
            return _mapper.Map<IEnumerable<PostalDeviceDto>>(postalDevices);
        }

        [HttpGet("{postalIdentifier}")]
        public async Task<IActionResult> GetPostageDeviceInformation(string postalIdentifier)
        {
            var postalDevice = await _postalDeviceService.GetPostageDeviceInformation(postalIdentifier);
            if (postalDevice == null) return NotFound();
            var dto = _mapper.Map<PostalDeviceDto>(postalDevice);
            return Ok(dto);
        }

        [HttpPut("{postalIdentifier}/withdraw")]
        public async Task<IActionResult> WithdrawPostalIdentifier(string postalIdentifier)
        {
            await _postalDeviceService.WithdrawPostalIdentifier(postalIdentifier);
            return Ok(true);
        }
    }

}
