﻿using Microsoft.AspNetCore.Mvc;
using PostageDevice.Service.DTOs;
using PostageDevice.Service.Interfaces;

namespace PostageDevice.Controller.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StampController : ControllerBase
    {
        private readonly IStampService _stampService;

        public StampController(IStampService stampService)
        {
            _stampService = stampService;
        }

        [HttpPost("generateStamp")]
        public async Task<IActionResult> GenerateStamp([FromBody] StampRequestDto requestDto)
        {
            var stampResponseDto = await _stampService.GenerateStampAsync(requestDto);
            if (stampResponseDto != null)
            {
                return Ok(stampResponseDto);
            }
            return BadRequest("Could not generate the stamp.");
        }
    }

}
