using PostageDevice.Infrastructure.InfrastructureDI;
using PostageDevice.Service.Mapping;
using PostageDevice.Service.ServiceDI;
using PostageDevice.ImprintApiSdk.ImprintApi;
using PostageDevice.Infrastructure.DbContext;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// config
ConfigurationManager configuration = builder.Configuration;

builder.Services.AddControllers();
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddAutoMapper(typeof(MappingProfile).Assembly);
builder.Services.AddHttpClient();


builder.Services.AddPostageDeviceDbContext(configuration);

builder.Services.AddInfrastructureDI();
builder.Services.AddDomainService();

builder.Services.AddImprintApiSdkDI();


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
