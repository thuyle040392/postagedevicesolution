﻿namespace PostageDevice.Service.DTOs
{
    public class StampResponseDto
    {
        public string StampImageBase64 { get; set; }
    }
}
