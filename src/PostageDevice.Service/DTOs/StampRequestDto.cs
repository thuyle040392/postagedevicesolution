﻿namespace PostageDevice.Service.DTOs
{
    public class StampRequestDto
    {
        public string PostalIdentifier { get; set; }
        public int ItemCount { get; set; }
        public decimal PostageValue { get; set; }
    }
}
