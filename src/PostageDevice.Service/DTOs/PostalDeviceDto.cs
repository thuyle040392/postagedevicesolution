﻿namespace PostageDevice.Service.DTOs
{
    public class PostalDeviceDto
    {
        public string PostalIdentifier { get; set; }
    }
}
