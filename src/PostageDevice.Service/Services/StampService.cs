﻿using PostageDevice.Core.Emuns;
using PostageDevice.Core.Interfaces;
using PostageDevice.ImprintApiSdk.ImprintApi;
using PostageDevice.Service.DTOs;
using PostageDevice.Service.Interfaces;
using System.Net;
using System.Text.Json;

namespace PostageDevice.Service.Services
{
    public class StampService : IStampService
    {
        private readonly IImprintApiClient _imprintApiClient;
        private readonly IPostalDeviceRepository _postalDeviceRepository;
        private readonly JsonSerializerOptions _serializerOptions;

        public StampService(IImprintApiClient imprintApiClient, IPostalDeviceRepository postalDeviceRepository)
        {
            _imprintApiClient = imprintApiClient;
            _postalDeviceRepository = postalDeviceRepository;
            _serializerOptions = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

        }

        public async Task<StampResponseDto> GenerateStampAsync(StampRequestDto stampRequest)
        {
            var postalDevice = await _postalDeviceRepository.GetByPostalIdentifierAsync(stampRequest.PostalIdentifier);
            if (postalDevice == null)
            {
                throw new InvalidOperationException("Postal Device not found.");
            }

            if (postalDevice.Status != Status.Installed)
            {
                throw new InvalidOperationException("Stamps can only be generated when the postage device's status is 'Installed'.");
            }
            try
            {
                var imprintResponse = await _imprintApiClient.GenerateImprintAsync(stampRequest.PostalIdentifier, stampRequest.ItemCount, stampRequest.PostageValue);
                if (imprintResponse.IsSuccessStatusCode)
                {
                    var responseBody = await imprintResponse.Content.ReadAsStringAsync();
                    var stampResponseDto = JsonSerializer.Deserialize<StampResponseDto>(responseBody, _serializerOptions);
                    if(stampResponseDto != null)
                    {
                        // UpdateRegistersAsync
                        await _postalDeviceRepository.UpdateRegistersAsync(postalDevice, stampRequest.ItemCount,stampRequest.PostageValue);
                        return stampResponseDto;
                    }
                    throw new InvalidOperationException("Response content is null or empty.");
                }
                else if (imprintResponse.StatusCode == HttpStatusCode.Forbidden)
                {
                    // Handle Forbidden by update status to Blocked
                    await _postalDeviceRepository.UpdateStatusAsync(postalDevice, Status.Blocked);
                    throw new InvalidOperationException("The Postal Device has been blocked.");
                }
                else
                {
                    throw new InvalidOperationException("Failed to generate stamp.");
                }
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("An error occurred while generating the stamp.", ex);
            }
        }
    }

}
