﻿using PostageDevice.Core.Entities;
using PostageDevice.Core.Interfaces;

namespace PostageDevice.Service.Services
{
    public class PostalDeviceService : IPostalDeviceService
    {
        private readonly IPostalDeviceRepository _postalDeviceRepository;

        public PostalDeviceService(IPostalDeviceRepository postalDeviceRepository)
        {
            _postalDeviceRepository = postalDeviceRepository;
        }

        public async Task<PostalDevice> DeclareNewPostageDevice(string postalIdentifier)
        {
            // Start a new transaction
            using var transaction = await _postalDeviceRepository.BeginTransactionAsync();
            try
            {
                var exists = await _postalDeviceRepository.GetByPostalIdentifierAsync(postalIdentifier);
                if (exists != null)
                {
                    throw new InvalidOperationException("A Postal Device with the given identifier already exists.");
                }
                var postalDevice = new PostalDevice(postalIdentifier);
                await _postalDeviceRepository.AddAsync(postalDevice);
                await transaction.CommitAsync();

                return postalDevice;
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                throw; 
            }
        }

        public async Task<IEnumerable<PostalDevice>> GetListOfPostageDevices()
        {
            return await _postalDeviceRepository.GetAllAsync();
        }

        public async Task<PostalDevice> GetPostageDeviceInformation(string postalIdentifier)
        {
            return await _postalDeviceRepository.GetByPostalIdentifierAsync(postalIdentifier);
        }

        public async Task WithdrawPostalIdentifier(string postalIdentifier)
        {
            var postalDevice = await _postalDeviceRepository.GetByPostalIdentifierAsync(postalIdentifier);
            if(postalDevice == null)
            {
                throw new InvalidOperationException("PostalDevice is null .");
            }
            postalDevice.Withdraw();
            await _postalDeviceRepository.UpdateAsync(postalDevice);
        }
    }
}
