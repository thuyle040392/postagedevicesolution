﻿using PostageDevice.Service.DTOs;

namespace PostageDevice.Service.Interfaces
{
    public interface IStampService
    {
        Task<StampResponseDto> GenerateStampAsync(StampRequestDto request);
    }
}
