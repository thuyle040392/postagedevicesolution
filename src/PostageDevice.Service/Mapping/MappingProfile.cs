﻿using AutoMapper;
using PostageDevice.Core.Entities;
using PostageDevice.Service.DTOs;

namespace PostageDevice.Service.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<PostalDevice, PostalDeviceDto>();
        }
    }
}
