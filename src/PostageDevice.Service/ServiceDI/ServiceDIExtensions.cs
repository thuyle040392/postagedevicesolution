﻿using Microsoft.Extensions.DependencyInjection;
using PostageDevice.Core.Interfaces;
using PostageDevice.Service.Interfaces;
using PostageDevice.Service.Services;

namespace PostageDevice.Service.ServiceDI
{
    public static class ServiceDIExtensions
    {
        public static void AddDomainService(this IServiceCollection service)
        {
            service.AddTransient<IPostalDeviceService, PostalDeviceService>();
            service.AddTransient<IStampService, StampService>();
        }
    }
}
