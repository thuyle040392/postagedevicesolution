﻿using Microsoft.Extensions.DependencyInjection;
using PostageDevice.Core.Interfaces;
using PostageDevice.Infrastructure.Repositories;

namespace PostageDevice.Infrastructure.InfrastructureDI
{
    public static class InfrastructureDIExtensions
    {
        public static void AddInfrastructureDI(this IServiceCollection service)
        {
            service.AddTransient<IPostalDeviceRepository, PostalDeviceRepository>();
        }
    }
}
