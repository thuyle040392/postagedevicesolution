﻿using Microsoft.EntityFrameworkCore;
using PostageDevice.Core.Entities;

namespace PostageDevice.Infrastructure.EfDbContext
{
    public class PostageDeviceDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public PostageDeviceDbContext(DbContextOptions<PostageDeviceDbContext> options) : base(options)
        {
        }
        public DbSet<PostalDevice> PostalDevices { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<PostalDevice>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasIndex(u => u.PostalIdentifier).IsUnique();
            });


        }
    }
}
