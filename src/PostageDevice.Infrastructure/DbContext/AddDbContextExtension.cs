﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PostageDevice.Infrastructure.EfDbContext;

namespace PostageDevice.Infrastructure.DbContext
{
    public static class AddDbContextExtension
    {
        public static void AddPostageDeviceDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<PostageDeviceDbContext>(options =>
            {
                options.UseSqlServer(connectionString);
            });
        }
    }
}
