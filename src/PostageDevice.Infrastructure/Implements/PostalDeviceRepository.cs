﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using PostageDevice.Core.Emuns;
using PostageDevice.Core.Entities;
using PostageDevice.Core.Interfaces;
using PostageDevice.Infrastructure.EfDbContext;

namespace PostageDevice.Infrastructure.Repositories
{
    public class PostalDeviceRepository : IPostalDeviceRepository
    {
        private readonly PostageDeviceDbContext _context;

        public PostalDeviceRepository(PostageDeviceDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(PostalDevice postalDevice)
        {
            await _context.PostalDevices.AddAsync(postalDevice);
            await _context.SaveChangesAsync();
        }

        public async Task<IDbContextTransaction> BeginTransactionAsync()
        {
            return await _context.Database.BeginTransactionAsync(); ;
        }

        public async Task<IEnumerable<PostalDevice>> GetAllAsync()
        {
            return await _context.PostalDevices.ToListAsync();
        }

        public async Task<PostalDevice> GetByPostalIdentifierAsync(string postalIdentifier)
        {
            return await _context.PostalDevices.SingleOrDefaultAsync(pd => pd.PostalIdentifier == postalIdentifier);
        }

        public async Task UpdateAsync(PostalDevice postalDevice)
        {
            _context.PostalDevices.Update(postalDevice);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateRegistersAsync(PostalDevice postalDevice, int itemCount, decimal postageValue)
        {
            postalDevice.UpdateRegisters(itemCount, postageValue);
            await UpdateAsync(postalDevice);
        }

        public async Task UpdateStatusAsync(PostalDevice postalDevice, Status newStatus)
        {
            postalDevice.UpdateStatus(newStatus);
            await UpdateAsync(postalDevice);
        }
    }
}
